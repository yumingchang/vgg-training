#!/usr/bin/env python2
# coding: utf-8

# In[1]:
########################################################################################
# Davi Frossard, 2016                                                                  #
# VGG16 implementation in TensorFlow                                                   #
# Details:                                                                             #
# http://www.cs.toronto.edu/~frossard/post/vgg16/                                      #
#                                                                                      #
# Model from https://gist.github.com/ksimonyan/211839e770f7b538e2d8#file-readme-md     #
# Weights from Caffe converted using https://github.com/ethereon/caffe-tensorflow      #
########################################################################################
from imagenet_classes import class_names
from skimage.transform import resize
from random import shuffle
from tqdm import tqdm
import tensorflow as tf
import numpy as np
import imageio
import time
import math
import sys
import os

# In[2]:
IMG_W = 224  # resize the image, if the input image is too large, training will be very slow.
IMG_H = 224
BATCH_SIZE = 64 #32-128
LEARNING_RATE = 1e-3 # with current parameters, it is suggested to use learning rate<0.0001
TOTAL_EPOCH = 1000
CLASSES = 2 # class size
ACC_BATCH_SIZE = 96
TEST_BATCH_SIZE = 96
CAPACITY = 1000


# In[3]:


class vgg16:
    #def __init__(self, imgs, labs, weights=None):
    def __init__(self, imgs, labs, weights=None):
        self.imgs = imgs
        self.labs = labs
        self.convlayers()
        self.fc_layers()
        self.prob = tf.nn.softmax(self.fc3l)
        self.cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=self.fc3l, labels=self.labs))
        self.train_op = tf.train.AdamOptimizer(LEARNING_RATE).minimize(self.cross_entropy)
        if weights is not None:
            self.load_weights(weights, self.sess)


    def convlayers(self):
        self.parameters = []

        # zero-mean input
        #with tf.name_scope('preprocess') as scope:
            #mean = tf.constant([123.68, 116.779, 103.939], dtype=tf.float32, shape=[1, 1, 1, 3], name='img_mean')
            #print mean.get_shape()
            #images = self.imgs - mean
        images = self.imgs
        tf.map_fn(lambda frame: tf.image.per_image_standardization(frame), images)

        # conv1_1
        with tf.name_scope('conv1_1') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 3, 64], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(images, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[64], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv1_1 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv1_2
        with tf.name_scope('conv1_2') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 64, 64], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv1_1, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[64], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv1_2 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # pool1
        self.pool1 = tf.nn.max_pool(self.conv1_2,
                               ksize=[1, 2, 2, 1],
                               strides=[1, 2, 2, 1],
                               padding='SAME',
                               name='pool1')

        # conv2_1
        with tf.name_scope('conv2_1') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 64, 128], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.pool1, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[128], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv2_1 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv2_2
        with tf.name_scope('conv2_2') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 128, 128], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv2_1, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[128], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv2_2 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # pool2
        self.pool2 = tf.nn.max_pool(self.conv2_2,
                               ksize=[1, 2, 2, 1],
                               strides=[1, 2, 2, 1],
                               padding='SAME',
                               name='pool2')

        # conv3_1
        with tf.name_scope('conv3_1') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 128, 256], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.pool2, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[256], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv3_1 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv3_2
        with tf.name_scope('conv3_2') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 256, 256], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv3_1, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[256], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv3_2 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv3_3
        with tf.name_scope('conv3_3') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 256, 256], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv3_2, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[256], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv3_3 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # pool3
        self.pool3 = tf.nn.max_pool(self.conv3_3,
                               ksize=[1, 2, 2, 1],
                               strides=[1, 2, 2, 1],
                               padding='SAME',
                               name='pool3')

        # conv4_1
        with tf.name_scope('conv4_1') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 256, 512], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.pool3, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv4_1 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv4_2
        with tf.name_scope('conv4_2') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv4_1, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv4_2 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv4_3
        with tf.name_scope('conv4_3') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv4_2, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv4_3 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # pool4
        self.pool4 = tf.nn.max_pool(self.conv4_3,
                               ksize=[1, 2, 2, 1],
                               strides=[1, 2, 2, 1],
                               padding='SAME',
                               name='pool4')

        # conv5_1
        with tf.name_scope('conv5_1') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.pool4, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv5_1 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv5_2
        with tf.name_scope('conv5_2') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv5_1, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv5_2 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv5_3
        with tf.name_scope('conv5_3') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv5_2, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv5_3 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # pool5
        self.pool5 = tf.nn.max_pool(self.conv5_3,
                               ksize=[1, 2, 2, 1],
                               strides=[1, 2, 2, 1],
                               padding='SAME',
                               name='pool4')

    def fc_layers(self):
        # fc1
        with tf.name_scope('fc1') as scope:
            shape = int(np.prod(self.pool5.get_shape()[1:]))
            fc1w = tf.Variable(tf.truncated_normal([shape, 4096],
                                                         dtype=tf.float32,
                                                         stddev=1e-1), name='weights')
            fc1b = tf.Variable(tf.constant(1.0, shape=[4096], dtype=tf.float32),
                                 trainable=True, name='biases')
            pool5_flat = tf.reshape(self.pool5, [-1, shape])
            fc1l = tf.nn.bias_add(tf.matmul(pool5_flat, fc1w), fc1b)
            self.fc1 = tf.nn.relu(fc1l)
            self.parameters += [fc1w, fc1b]

        # fc2
        with tf.name_scope('fc2') as scope:
            fc2w = tf.Variable(tf.truncated_normal([4096, 4096],
                                                         dtype=tf.float32,
                                                         stddev=1e-1), name='weights')
            fc2b = tf.Variable(tf.constant(1.0, shape=[4096], dtype=tf.float32),
                                 trainable=True, name='biases')
            fc2l = tf.nn.bias_add(tf.matmul(self.fc1, fc2w), fc2b)
            self.fc2 = tf.nn.relu(fc2l)
            self.parameters += [fc2w, fc2b]

        # fc3
        with tf.name_scope('fc3') as scope:
            fc3w = tf.Variable(tf.truncated_normal([4096, CLASSES],
                                                         dtype=tf.float32,
                                                         stddev=1e-1), name='weights')
            fc3b = tf.Variable(tf.constant(1.0, shape=[CLASSES], dtype=tf.float32),
                                 trainable=True, name='biases')
            self.fc3l = tf.nn.bias_add(tf.matmul(self.fc2, fc3w), fc3b)
            self.parameters += [fc3w, fc3b]

    def load_weights(self, weight_file, sess):
        weights = np.load(weight_file)
        keys = sorted(weights.keys())
        for i, k in enumerate(keys):
            print i, k, np.shape(weights[k])
            sess.run(self.parameters[i].assign(weights[k]))


# In[4]:


def next_batch(all_img, index_now, train_dir, batch_size):
    #all_img = os.listdir(train_dir)
    index_end = index_now + batch_size
    if index_end >= len( all_img ):
        index_now = 0
        index_end = batch_size
        shuffle( all_img )
        #perm = np.arange(len(train_dir))
        #np.random.shuffle(perm)
        #np.array(all_img)[perm]
    store_img = list()
    store_lab = list()
    cats = 0
    dogs = 0
    for img_name in all_img[index_now:index_end]:
        name = img_name.split('.')
        store_img.append(resize( imageio.imread( train_dir + img_name, pilmode='RGB' ), (224, 224) ) )
        if name[0] == 'cat':
            store_lab.append([1, 0])
            cats += 1
        else:
            store_lab.append([0, 1])
            dogs += 1
    index_now += batch_size
    store_img = np.asarray(store_img)
    store_lab = np.asarray(store_lab)
    #print "this batch contains " + str( cats ) + "cats and " + str( dogs ) + "dogs"
    return all_img, index_now, store_img, store_lab


# In[ ]:


if __name__ == '__main__':
    train_dir = '/notebooks/train-data/train/'
    test_dir = '/notebooks/train-data/test1/'

    train_size = len( os.listdir(train_dir) )
    acc_size = len( os.listdir(train_dir) )
    test_size = len( os.listdir(test_dir) )


    imgs = tf.placeholder(tf.float32, [None, 224, 224, 3] )
    labs = tf.placeholder(tf.float32, [None, CLASSES] )
    #vgg = vgg16( imgs, labs, 'vgg16_weights.npz')
    vgg = vgg16( imgs, labs )
    with tf.Session() as sess:
        init_op = tf.global_variables_initializer()
        last_min_loss = float("inf")
        last_max_acc_correct = 0
        last_max_test_correct = 0
        saver = tf.train.Saver()
        sess.run( init_op )
        index_train = 0
        index_test = 0
        index_acc = 0
        train_list = os.listdir(train_dir)
        acc_list = os.listdir(train_dir)
        test_list = os.listdir(test_dir)

        for epoch in range( TOTAL_EPOCH ):
            # training
            batch_per_epoch = int( math.ceil( train_size / BATCH_SIZE ) )
            #pbar = tqdm(total=batch_per_epoch)
            total_loss = 0.0
            start_time = time.time()
            for i in range( batch_per_epoch ):
                train_list, index_train, train_imgs, train_labs = next_batch( train_list, index_train, train_dir, BATCH_SIZE)
                _, loss = sess.run( [vgg.train_op, vgg.cross_entropy], feed_dict={vgg.imgs: train_imgs, vgg.labs: train_labs} )
                total_loss += loss
                #pbar.update(1)
            #pbar.close()
            total_loss = total_loss / batch_per_epoch
            print( "--- Epoch %s --- %s seconds ---" % ( ( epoch + 1 ), ( time.time() - start_time ) ) )
            print( "total loss : %s" % ( total_loss ) )
            if total_loss < last_min_loss:
                save_path = saver.save(sess, "./loss_min.ckpt")
            if epoch % 3 == 0:
                #training data set accuracy
                acc_batch_per_epoch = int( math.ceil( acc_size / ACC_BATCH_SIZE ) )
                #pbar = tqdm(total=acc_batch_per_epoch)
                correct_acc = 0
                acc_loss_sum = 0
                for i in range( acc_batch_per_epoch ):
                    acc_list, index_acc, acc_imgs, acc_labs = next_batch( acc_list, index_acc, train_dir, ACC_BATCH_SIZE )
                    acc_loss, acc_prob = sess.run([ vgg.cross_entropy, vgg.prob ], feed_dict={vgg.imgs: acc_imgs, vgg.labs: acc_labs} )
                    #print "--- train accuracy start ---"
                    for j in range ( ACC_BATCH_SIZE ):
                        acc_loss_sum += acc_loss
                        correct_acc += acc_labs[ j ][ int( np.argmax( acc_prob[j] ) ) ]
                    #pbar.update(1)
                #pbar.close()
                if correct_acc > last_max_acc_correct:
                    save_path = saver.save(sess, "./best_acc.ckpt")
                print( "train data set accuracy : %s" % ( float( correct_acc ) / float( acc_batch_per_epoch * ACC_BATCH_SIZE ) ) )
            # test
            if epoch % 9 == 0:
                test_batch_per_epoch = int( math.ceil( test_size / TEST_BATCH_SIZE ) )
                #pbar = tqdm( total=test_batch_per_epoch )
                correct = 0
                loss_sum = 0
                for i in range( test_batch_per_epoch ):
                    test_list, index_test, test_imgs, test_labs = next_batch( test_list, index_test, test_dir, TEST_BATCH_SIZE )
                    test_loss, test_prob = sess.run([ vgg.cross_entropy, vgg.prob ], feed_dict={vgg.imgs: test_imgs, vgg.labs: test_labs} )
                    #print "--- train accuracy start ---"
                    for j in range ( TEST_BATCH_SIZE ):
                        loss_sum += test_loss
                        correct += test_labs[ j ][ int( np.argmax( test_prob[ j ] ) ) ]
                    #pbar.update(1)
                #pbar.close()
                if correct > last_max_test_correct:
                    save_path = saver.save(sess, "./best_test.ckpt")
                print( "test data set accuracy : %s" % ( float( correct ) / float( test_batch_per_epoch * TEST_BATCH_SIZE ) ) )
            print( "--- Epoch %s --- end ---" % ( ( epoch + 1 ) ) )
